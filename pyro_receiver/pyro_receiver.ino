/*
  Operator remote pyrocutter
*/
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3);
const int SW1 = 5;
const int SW2 = 6;
const int SW3 = 7;
const int SW4 = 8;
const int SW5 = 9;

void setup() {
  Serial.begin(115200);
  mySerial.begin(115200);
  pinMode(SW1, OUTPUT);
  pinMode(SW2, OUTPUT);
  pinMode(SW3, OUTPUT);
  pinMode(SW4, OUTPUT);
  pinMode(SW5, OUTPUT);
}

void loop() {
  if (mySerial.available()) {
    char buf[32];
    for( int i = 0; i < mySerial.available(); i++ ) {
      char call = mySerial.read();
      String data = String(call);
      if (data.substring(0) == "1") digitalWrite(SW1, HIGH);
      if (data.substring(0) == "2") digitalWrite(SW2, HIGH);
      if (data.substring(0) == "3") digitalWrite(SW3, HIGH);
      if (data.substring(0) == "4") digitalWrite(SW4, HIGH);
      if (data.substring(0) == "5") digitalWrite(SW5, HIGH);
      
      Serial.print(data);
    }


    

  }
}
