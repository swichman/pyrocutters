# pyrocutters

Transmitter and Receiver software for a remote pyro cutter.

## Receiver
There are 5 activation signals. All remote receivers will actuate all 5 signals, this way there is flexibility as to which signals trigger which pyrocutters. The pins of the receiver are as follows:


| D8  | D9  | D10 | D11 | D12 |
| --- | --- | --- | --- | --- |
| SW1 | SW2 | SW3 | SW4 | SW5 |

The Tx/Rx pair from the telemetry radio connect to a software serial port on pins D2 | D3. This allows for the usb-serial connection to allow debug of received signals.

## Transmitter

This telemetry radio is also connected to a software serial port on pins D2 | D3. This allows for the usb-serial port to be used as a debug port to ensure that the proper signals are sent on button presses. The telemetry radio is set to be the master node. The way these radios work on a multinode network for this application is that the transmitter broadcasts the button pressed to all receiver nodes. The receiver nodes then send a signal high based on the received pin number.
