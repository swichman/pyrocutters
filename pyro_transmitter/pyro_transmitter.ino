/*
  Operator remote pyrocutter
*/
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3);
const int SW1 = 5;
const int SW2 = 6;
const int SW3 = 7;
const int SW4 = 8;
const int SW5 = 9;


void setup() {
  mySerial.begin(115200);
  pinMode(SW1, INPUT);
  pinMode(SW2, INPUT);
  pinMode(SW3, INPUT);
  pinMode(SW4, INPUT);
  pinMode(SW5, INPUT);
}

void loop() {
  int btn[5] = {SW1, SW2, SW3, SW4, SW5};
  for( int i = 0; i < 5; i++ ) {
    if( digitalRead(btn[i]) == HIGH ) {
      int sw = i + 1;
      char buffer[40];
      sprintf(buffer, "%d%d%d\n\r", sw, sw, sw);
      mySerial.write(buffer);
    }
  }
  delay(100);
}
